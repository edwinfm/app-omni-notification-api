package com.omni.notification.integrations.servicebus;

import com.google.gson.Gson;
import com.omni.notification.commons.utils.EmailUtil;
import com.omni.notification.configs.RabbitConfig;
import com.omni.notification.integrations.model.NotificationModel;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class QueueConsumer {

    @Value("${credenciales.host}")
    private String host;

    @Value("${credenciales.port}")
    private Integer port;

    @Value("${credenciales.username}")
    private String user;

    @Value("${credenciales.from}")
    private String from;

    @Value("${credenciales.password}")
    private String password;

    @RabbitListener(queues = RabbitConfig.QUEUE_NAME, containerFactory = "customRabbitListenerContainerFactory")
    @RabbitHandler
    public void onMessageFromRabbitMQ(@Payload String payload, Message message, Channel channel) {
        try {
            log.info( payload, message, channel );
            Gson gson = new Gson();
            NotificationModel notification = gson.fromJson(payload, NotificationModel.class);
            notification.setFrom(from);
            notification.setUsername(user);
            notification.setPassword(password);
            notification.setHost(host);
            notification.setPort(port);
            EmailUtil.sendMail(notification);
        } catch (Exception e) {
            log.error("Se presentó un fallo en el proceso", e);
        }
    }

}
