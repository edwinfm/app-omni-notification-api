package com.omni.notification.integrations.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class NotificationModel implements Serializable {
    private static final long serialVersionUID = -4656669925948794779L;

    private String from;
    private String username;
    private String password;
    private String host;
    private int port;
    private String to;
    private String subject;
    private String text;
}