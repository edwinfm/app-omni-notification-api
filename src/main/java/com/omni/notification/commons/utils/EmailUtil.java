package com.omni.notification.commons.utils;

import com.omni.notification.integrations.model.NotificationModel;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Component
public class EmailUtil {

    public static void sendMail( NotificationModel model ) {
        try {
            Session session = obtainSession(model);
            MimeBodyPart htmlPart = new MimeBodyPart();
            MimeMultipart multiParte = new MimeMultipart();
            Transport transport = session.getTransport("smtp");

            htmlPart.setContent(model.getText(), "text/html; charset=utf-8");
            multiParte.addBodyPart(htmlPart);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(model.getFrom()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(model.getTo()));
            message.setSubject(model.getSubject());
            message.setContent(multiParte);

            transport.connect(model.getHost(), model.getUsername(), model.getPassword());
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private static Session obtainSession( NotificationModel model ) {
        Properties props = obtainProperties(model);
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);
        return session;
    }

    private static Properties obtainProperties(NotificationModel model) {
        Properties props = new Properties();
        props.put("mail.smtp.host", model.getHost());
        props.put("mail.smtp.user", model.getUsername());
        props.put("mail.smtp.clave", model.getPassword());
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", Integer.toString(model.getPort()));
        props.put("mail.smtp.auth", "true");
        props.put("mail.transport.protocol", "smtp");
        return props;
    }
}
